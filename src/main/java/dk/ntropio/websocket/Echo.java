package dk.ntropio.websocket;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/echo")
public class Echo {
    private static Set<Session> clients = Collections.synchronizedSet(new HashSet<Session>());
 
    @OnOpen
    public void connect(Session session){
        clients.add(session);        
        System.out.println("Session: " + session);
    }
    
    @OnClose
    public void close(Session session){
        clients.remove(session);       
        System.out.println("Close");
    }
    
    @OnMessage
    public void onMessage(String message, Session session) throws IOException{
    
        synchronized(clients){
            for (Session client : clients) {
                if (!client.equals(session)) {
                    client.getBasicRemote().sendText(message);
                }
            }
        }
    }
}
